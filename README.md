# nRF Command Line Tools Nix Flake
Useful for creating development environments requiring these tools.

## Installation Extras
The Segger JLink pack requires the `99-jlink.rules` udev file to be added to `/etc/udev/rules.d` in order to function.  I believe this can be done relatively trivially using NixOS's `environment.etc` option, but for those of us not using NixOS, here's the excerpt from the README:
```
Running J-Link/ Flasher software using J-Link or Flasher via USB with standard user rights
=========================================================================
In order to run J-Link/ Flasher software with standard user rights you have to do the following:

- Copy the file "99-jlink.rules" provided with this software package 
  in the /etc/udev/rules.d/ directory using this command:
  
  sudo cp 99-jlink.rules /etc/udev/rules.d/
  
  Note: For older systems it might be necessary to replace the "ATTRS" calls in the 99-jlink.rules by "SYSFS" calls

- Either restart your system or manually trigger the new rules with the following commands:

  sudo udevadm control -R
  sudo udevadm trigger --action=remove --attr-match=idVendor=1366 --subsystem-match=usb
  sudo udevadm trigger --action=add    --attr-match=idVendor=1366 --subsystem-match=usb
``` 

## Development

### Building the flake outputs
```bash
# to build the Segger JLink pack separately
nix build .#segger-jlink-pack

# to build the entire command line tools (including JLink)
nix build .#nrf-command-line-tools
```

### Testing the flake outputs
```bash
nix shell .#segger-jlink-pack

nix shell .#nrf-command-line-tools
```

### Resources
- [Segger JLink Software and Documentation pack](https://www.segger.com/downloads/jlink/#J-LinkSoftwareAndDocumentationPack)
- [Nordic Command Line Tools main page](https://www.nordicsemi.com/Products/Development-tools/nRF-Command-Line-Tools)
- [Nordic Command Line Tools documentation page](https://infocenter.nordicsemi.com/index.jsp?topic=%2Fug_nrf_cltools%2FUG%2Fcltools%2Fnrf_command_line_tools_lpage.html)
