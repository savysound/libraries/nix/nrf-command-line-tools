{ lib, stdenv, fetchurl, callPackage, makeWrapper }:

let
  version = "10.24.2";

  src = fetchurl {
    url = "https://nsscprodmedia.blob.core.windows.net/prod/software-and-other-downloads/desktop-software/nrf-command-line-tools/sw/versions-10-x-x/10-24-2/nrf-command-line-tools-${version}_linux-amd64.tar.gz";
    sha256 = "sha256-nWWw2A/MtWdmj6QVhckmGUexdA9S66jTo2jPb4/Xt5M=";
  };
in rec {
  segger-jlink-pack =
    let
      version = "V794e";
      jlinkPath = "JLink_Linux_${version}_x86_64";
    in
    stdenv.mkDerivation {
      pname = "segger-jlink-pack";
      inherit version src;

      postUnpack = ''
        tar xvf ${jlinkPath}.tgz
      '';

      installPhase = ''
        mkdir -p $out
        cp -r ../${jlinkPath}/* $out
      '';

      meta = with lib; {
        description = "J-Link Software and Documentation pack: All-in-one debugging solution";
        homepage = "https://www.segger.com/downloads/jlink/#J-LinkSoftwareAndDocumentationPack";
        platforms = platforms.unix;
      };
    };

  nrf-command-line-tools =
    stdenv.mkDerivation {
      pname = "nrf-command-line-tools";
      inherit version src;

      nativeBuildInputs = [ makeWrapper ];
      buildInputs = [ segger-jlink-pack ];

      installPhase = ''
        mkdir -p $out
        cp -r * $out

        # wrap nrfjprog to to manually link to the segger shared object
        wrapProgram $out/bin/nrfjprog \
          --add-flags "--jdll ${segger-jlink-pack}/libjlinkarm.so"
      '';

      meta = with lib; {
        description = "To program Nordic Semiconductor SoCs through SEGGER J-Link programmers and debuggers.";
        homepage = "https://www.nordicsemi.com/Products/Development-tools/nRF-Command-Line-Tools";
        platforms = platforms.unix;
      };
    };
}
