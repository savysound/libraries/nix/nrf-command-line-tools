{
  description = "nRF Command Line Tools for Nordic Semiconductor";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
  };

  outputs = { self, nixpkgs }:
    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
    in {
      packages.x86_64-linux = pkgs.callPackage ./. { };
    };
}
